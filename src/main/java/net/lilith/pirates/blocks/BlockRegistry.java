package net.lilith.pirates.blocks;

import net.lilith.pirates.Pirates;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class BlockRegistry {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, Pirates.MODID);

    public static final RegistryObject<Block> OAK_RAIL = BLOCKS.register("oak_rail", RailingBlock::new);
}
