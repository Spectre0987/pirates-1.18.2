package net.lilith.pirates.items;

import net.lilith.pirates.Pirates;
import net.lilith.pirates.blocks.BlockRegistry;
import net.lilith.pirates.creativetabs.Tabs;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.Tiers;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ItemRegistry {

    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, Pirates.MODID);

    public static final RegistryObject<Item> CUTLASS = ITEMS.register("cutlass", () -> new SwordItem(Tiers.IRON, 3, -2.4F, new Item.Properties().stacksTo(1)));


    //BlockItems
    public static final RegistryObject<Item> OAK_RAILING = createBlockItem(BlockRegistry.OAK_RAIL);

    public static RegistryObject<Item> createBlockItem(RegistryObject<Block> block, Item.Properties prop){
        return ITEMS.register(block.getId().getPath(), () -> new BlockItem(block.get(), prop));
    }

    public static RegistryObject<Item> createBlockItem(RegistryObject<Block> block){
        return createBlockItem(block, new Item.Properties().tab(Tabs.MAIN));
    }
}
