package net.lilith.pirates;

import net.lilith.pirates.blocks.BlockRegistry;
import net.lilith.pirates.items.ItemRegistry;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(Pirates.MODID)
public class Pirates {

    public static final String MODID = "lilithpirates";

    public Pirates(){
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();

        ItemRegistry.ITEMS.register(bus);
        BlockRegistry.BLOCKS.register(bus);
    }
}
