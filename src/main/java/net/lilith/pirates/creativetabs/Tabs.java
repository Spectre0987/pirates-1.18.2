package net.lilith.pirates.creativetabs;

import net.lilith.pirates.blocks.BlockRegistry;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;

public class Tabs {

    public static final CreativeModeTab MAIN = new CreativeModeTab("") {

        @Override
        public ItemStack makeIcon() {
            return new ItemStack(BlockRegistry.OAK_RAIL.get());
        }
    };

}
