package net.lilith.pirates.data;

import net.lilith.pirates.Pirates;
import net.lilith.pirates.blocks.BlockRegistry;
import net.lilith.pirates.creativetabs.Tabs;
import net.minecraft.data.DataGenerator;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraftforge.common.data.LanguageProvider;

public class PirateLangProvider extends LanguageProvider {
    public PirateLangProvider(DataGenerator gen) {
        super(gen, Pirates.MODID, "en_us");
    }

    @Override
    protected void addTranslations() {
        this.add(Tabs.MAIN, "Pirates Tab");

        this.add(BlockRegistry.OAK_RAIL.get(), "Oak Railing");
    }

    public void add(CreativeModeTab tab, String trans){
        if(tab.getDisplayName() instanceof TranslatableComponent)
            add(((TranslatableComponent)tab.getDisplayName()).getKey(), trans);
    }
}
