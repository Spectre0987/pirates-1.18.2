package net.lilith.pirates.data;

import net.lilith.pirates.Pirates;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.forge.event.lifecycle.GatherDataEvent;

@Mod.EventBusSubscriber(modid = Pirates.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGen {

    @SubscribeEvent
    public static void gatherData(GatherDataEvent event){
        event.getGenerator().addProvider(new PirateLangProvider(event.getGenerator()));
        event.getGenerator().addProvider(new PiratesBlockStateProvider(event.getGenerator(), event.getExistingFileHelper()));
    }
}
