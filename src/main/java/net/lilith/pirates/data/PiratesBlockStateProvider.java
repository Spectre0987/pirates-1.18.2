package net.lilith.pirates.data;

import net.lilith.pirates.Pirates;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

public class PiratesBlockStateProvider extends BlockStateProvider {

    public PiratesBlockStateProvider(DataGenerator gen, ExistingFileHelper exFileHelper) {
        super(gen, Pirates.MODID, exFileHelper);
    }

    @Override
    protected void registerStatesAndModels() {

    }
}
